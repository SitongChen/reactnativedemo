import React from 'react';
import { Dimensions, StyleSheet, Text, View, FlatList , SectionList } from 'react-native';
import Measurer from './Measurer.js';
import BasicFlatList from './components/BasicFlatList';
import StatusBar from './components/StatusBar';
import MainWindow from './components/MainWindow';
import EditWindow from './components/EditViewController';

import {StackNavigator} from 'react-navigation';

const Stack = StackNavigator({
  Home : {screen : MainWindow },
  Edit : {screen : EditWindow}, 
});


export default class App extends React.Component {

  callback(textList, heights) {
    console.log(textList);
    console.log(heights);

  }
  render() {
    return (
        <Stack />
    );
  }
}

// export default class App extends React.Component {

//   callback(textList, heights) {
//     console.log(textList);
//     console.log(heights);

//   }
//   render() {
//     return (
//       <View>
//         <StatusBar />

//         <BasicFlatList />
//       </View>
//     );
//   }
// }

const styles = StyleSheet.create({


  container: {
    paddingTop: 22,
    flex: 1,
    backgroundColor: '#ff0',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
    sectionHeader: {
    flex: 1,
    paddingTop: 20,
    paddingLeft: 10,
    paddingRight: 10,
    paddingBottom: 2,
    fontSize: 14,
    fontWeight: 'bold',
    backgroundColor: 'rgba(247,247,247,1.0)',
  },

  item: {
    padding: 10,
    fontSize: 18,
    height: 40,
    width: Dimensions.get('window').width,
    backgroundColor: '#f00',

  },
});
