import React from 'react';
import {NavigationActions} from 'react-navigation';
import { Dimensions, StyleSheet, Text, View, FlatList , SectionList, Image, TextInput , Button, Alert} from 'react-native';

var randomString = require('random-string');
const tempUri = "https://upload.wikimedia.org/wikipedia/commons/b/bf/Cornish_cream_tea_2.jpg";

export default class EditWindow extends React.Component{

	constructor(props){
		super(props);
		this.state = {
			name: '',
			description: '',
		}
	}
	static navigationOptions =  ({ navigation }) =>{
		return {
			title: "Product",
			// headerRight: (
			//     <Button 
   //                  title='Done' 
   //                  onPress={ () => navigation.dispatch(NavigationActions.back()) }                
   //                  backgroundColor= "rgba(0,0,0,0)"
   //                  color="rgba(0,122,255,1)"
   //              />   
			// ),
		};

	};


	returnData(){
		console.log(randomString({length: 24}));
		if (this.state.name == '' || this.state.description == ''){
			Alert.alert(
				'Notice',
				'Name or description is empty.',
				[
					{text: 'OK', onPress: () => console.log('OK Pressed')},
				]
				);
			return
		}
		this.props.navigation.state.params.returnData({name: this.state.name, description: this.state.description, url: tempUri, key: randomString({length: 24})});
		this.props.navigation.goBack();
	}

	render(){
		return (
			<View>
				<Image source={{uri:tempUri}} style={{width: Dimensions.get('window').width, height: Dimensions.get('window').width}}>
				</Image>

				<View style={{alignItems: 'center', height : 40, flexDirection: 'row', backgroundColor: 'rgba(0,122,255,1)', padding: 5}}>
					<Text > Name </Text>
					<TextInput 
						style={{flex: 1, borderWidth: 1 , borderColor: 'red'}}
						onChangeText={(text) => this.setState({description: text})}

					/>

				</View>

				<View style={{alignItems: 'center', height : 40, flexDirection: 'row', backgroundColor: 'rgba(0,122,255,1)', padding: 5}}>
					<Text > Description </Text>
					<TextInput 
						style={{flex: 1, borderWidth: 1 , borderColor: 'red'}}
						onChangeText={(text) => this.setState({name: text})}
					/>
				</View>

				<Button style={{backgroundColor: 'black', orderWidth: 1, borderRadius: 4, borderColor:'blue'}} title='Submit' onPress={() => this.returnData() } />
			</View>
			);
	}

}