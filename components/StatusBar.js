import React, {Component} from 'react';
import { StyleSheet, Text, View, Platform} from 'react-native';

class StatusBar extends Component{
	render(){
		return (
			<View style = {styles.statusbar}>

			</View>
			);
	}
}

const styles = StyleSheet.create({
	statusbar: {
		height: (Platform.OS === 'ios') ? 20 : 0,
		backgroundColor: "white",
	}
});

module.exports = StatusBar;