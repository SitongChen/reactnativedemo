
import React, {Component} from 'react';
import { Alert, Dimensions, StyleSheet, Text, View, FlatList , SectionList , Image, Platform} from 'react-native';
import Swipeout from 'react-native-swipeout';

// import flatListData from '../Data/flatListData';

class FlatListItem extends Component{

	render(){
		const swipeSetting={
			autoClose: true,
			onClose:(secId, rowId, direction) => {

			},
			onOpen:(secId, rowId, direction) => {

			},
			right: [
				{
					onPress: () => {
						Alert.alert(
							'Alert',
							'Are u sure you want to delete?',
							[
								{text:'No' , onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
								{text:'Yes' , onPress: () => { 
									flatListData.splice(this.props.index, 1);
									this.props.parent.listRefresh(this.props.item.key);
									 }},
							]
							);
					},
					text: 'Delete',
					type: 'delete'
				},
				{
					onPress: () => {
						this.props.parent.nav();
					},
					text: 'Edit',
					type: 'edit'
				}
			],
			rowId: this.props.index,
			sectionId: 1
		}
		return (
			<Swipeout {...swipeSetting}>
				<View style={{
					flex: 1,
					backgroundColor: "tomato",
				}}>
					<View style={{flex:1, flexDirection:'row'}}>
						<Image source={{uri:this.props.item.imageUrl}} style={{width:100, height:100, margin:5}} >
						</Image>
						<View style={{flex:1, flexDirection:'column', paddingTop:10}}>
							<Text style={{fontSize: 20}}> {this.props.item.name} </Text>
							<Text style={{color: 'gray', fontSize: 15}}> {this.props.item.foodDescription} </Text>
						</View>
					</View>

					<View style={{height:1, backgroundColor:"black"}}>

					</View>



				</View>
			</Swipeout>

			);
	}
}

export default class BasicFlatList extends Component{

	constructor(props){
		super(props);
		this.state = {
			key: null
		}
	}
	listRefresh( key ){
		console.log(key);
		this.setState({key : key});
	}

	nav(){
		this.props.parent.nav();
	}
	render(){
		return (
			<View style={styles.main}>
				<FlatList
					data = {this.props.flatListData}
					renderItem = {({item, index}) => {
						console.log(JSON.stringify(item));

						return (
							<FlatListItem item={item} index={index} parent={this}/>
							);
					}}
				>
				</FlatList>
			</View>
			);
	}
}

const styles = StyleSheet.create({
	main:{
		height: Dimensions.get('window').height - ((Platform.OS === 'ios') ? 20 : 0),
	},
	container: {
		flex: 1,
		padding: 10
	},

});