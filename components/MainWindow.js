import React from 'react';
import { Button, TouchableHighlight,Dimensions, StyleSheet, Text, View, FlatList , SectionList } from 'react-native';
import BasicFlatList from './BasicFlatList';
import StatusBar from './StatusBar';
import flatListData from '../Data/flatListData';


export default class MainWindow extends React.Component {

  constructor(props){
    super(props);
    this.state = {
      flatListData: flatListData
    }
  }
  static navigationOptions = ({ navigation }) => {
          return {
              title: 'FEED',
              headerRight: (
                <Button 
                    title='Add' 
                    onPress={ () => navigation.state.params.push() }                
                    backgroundColor= "rgba(0,0,0,0)"
                    color="rgba(0,122,255,1)"
                />              ), 


              tabBarIcon: ({ tintColor}) => {
                  return <Icon name = "favorite" size={26} color={tintColor} />;
              } 
          }
  };

  componentDidMount(){
    console.log("LOOOOOOO");
    this.props.navigation.setParams({
      push: this.nav.bind(this),
    });
  }


  nav(){
    this.props.navigation.navigate('Edit', {returnData: this.callbackFromChildScreen.bind(this)});
  }

  callbackFromChildScreen(data){
    console.log("Call from child screen.");
    console.log(data);
    this.setState( previousState => {
      return {flatListData : previousState.flatListData.concat({
        "key": data.key,
        "name": data.name,   
        "imageUrl": data.url,                    
        "foodDescription": data.description,
      })};
    });
  }


  render() {

    return (
      <View>

        <BasicFlatList parent={this} flatListData={this.state.flatListData}/>
      </View>
 

    );
  }
}

class EditButton extends React.Component{
  render(){
    return(
    <TouchableHighlight
      onPress={() => this.props.nav()} >
    <Text>Edit</Text>
  </TouchableHighlight>
      );
  }
}

const styles = StyleSheet.create({


  container: {
    paddingTop: 22,
    flex: 1,
    backgroundColor: '#ff0',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
    sectionHeader: {
    flex: 1,
    paddingTop: 20,
    paddingLeft: 10,
    paddingRight: 10,
    paddingBottom: 2,
    fontSize: 14,
    fontWeight: 'bold',
    backgroundColor: 'rgba(247,247,247,1.0)',
  },

  item: {
    padding: 10,
    fontSize: 18,
    height: 40,
    width: Dimensions.get('window').width,
    backgroundColor: '#f00',

  },
});
